#! /usr/bin/env python3

"""Make map of philosopher to the principal country they are
associated with.  When done, pickle the sep_cache."""

import pprint
import pickle

from main import countrylist, load_philosophers, trim_phil_list_for_SEP, make_principal_country_map, sep_cache

def main():
    phil_list_big = load_philosophers('philosophers.csv', -900, 2022) # get all philosophers
    phil_list = trim_phil_list_for_SEP(phil_list_big) # trim it to just those with SEP entries
    # phil_list = phil_list[:200]                       # FIXME: just for debugging
    country_map = make_principal_country_map(phil_list, countrylist)
    pprint.pprint(country_map)
    pprint.pprint(country_map.keys())
    pprint.pprint(country_map[('Socrates', 'Socrates')])    
    with open('country_map.pprint', 'w') as f:
        pprint.pprint(country_map, f)
    print('# just wrote country map to file country_map.pprint')
    with open('sep_cache.pickle', 'wb') as f:
        pickle.dump(sep_cache, f, pickle.HIGHEST_PROTOCOL)
    print('just wrote sep_cache to pickle file sep_cache.pickle')
    


if __name__ == '__main__':
    main()

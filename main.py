#! /usr/bin/env python3

"""philosopher_cities.py -- uses the Stanford Encyclopedia of
Philosophy to collect information about individual philosophers. It
then parses that information looking for names of cities and other 
philosophers to whichthese philosophers are associated.  
Uses very simplistic approaches.

"""

import os
import pprint
import pickle
import urllib.request
import matplotlib.pyplot as plt
from bs4 import BeautifulSoup


verbose = False
city_size_limit = 100000
ignore_pickled_data = False

# sometimes an article will say "was an English" instead of "was a
# British", so this AKA dictionary helps us implement a heuristic for
# that
AKA_countries = {'British' : ['English']}

# mapping of name to other possible names that might be used in the SEP
excluded_philosophers = {'I', 'Berlin', 'Wisdom'}
AKA_dict = {'Galilei' : 'Galileo',
            'Zeno of Elea' : 'zeno-elea',
            'Zeno of Citium' : 'zeno-citium',
            'David Lewis' : 'david-lewis',
            'Elisabeth of Bohemia' : 'elisabeth-bohemia',
            'Siddartha Gautama' : 'buddha'
            }
hardcoded_country_map = {'Hegel' : 'German',
                         'Alexander' : 'Australian',
                         'Anscombe' : 'Irish',
                         'Al-Kindi' : 'Arabic',
                         'Al-Farabi' : 'Arabic',
                         'Israeli' : 'Arabic',
                         'Machiavelli' : 'Italian',
                         'Campanella' : 'Italian',
                         'Quine' : 'American',
                         'Prior' : 'New_Zelander',
                         'Davidson' : 'American',
                         'Hartshorne' : 'American',
                         'Dewey' : 'American',
                         'Burley' : 'British',
                         'Hare' : 'British',
                         'Falaquera' : 'Spanish',
                         'Collingwood' : 'British',
                         'Whitehead' : 'British',
                         'Boethius' : 'Roman',
                         'Aquinas' : 'Italian',
                         'Desgabets' : 'French',
                         'Grosseteste' : 'British',
                         'Telesio' : 'Italian',
                         'Ficino' : 'Italian',
                         'Berlin' : 'Latvian',
                         'Benjamin' : 'German',
                         'Theophrastus' : 'Greek',
                         'Xenocrates' : 'Greek',
                         'Alyngton' : 'British',
                         'Olivi' : 'French',
                         'Wyclif' : 'British',
                         'Pomponazzi' : 'Italian',
                         'Arendt' : 'German',
                         'Bessarion' : 'Greek',
                         'Copernicus' : 'Polish',
                         'Cudworth' : 'British',
                         'Bonaventure' : 'Italian',
                         'Cardano' : 'Italian',
                         'Leibniz' : 'German',
                         'Abelard' : 'French',
                         'Luther' : 'German',
                         'Erasmus' : 'Dutch',
                         'Cockburn' : 'British',
                         'Ockham' : 'British',
                         'Wodeham' : 'British',
                         'Kilvington' : 'British',
                         'Gersonides' : 'French',
                         'Hobbes' : 'British',
                         'Ramsey' : 'British',
                         'Zabbarella' : 'Italian',
                         'Wittgenstein' : 'Austrian',
                         'Newton' : 'British',
                         'Schlegel' : 'German',
                         'Mill' : 'British',
                         'Green' : 'British',
                         'Kierkegaard' : 'Danish',
                         'Church' : 'American',
                         'Conway' : 'British',
                         'Anselm' : 'French', # Aosta was in France at that time
                         'Edwards' : 'American',
                         'Bolzano' : 'Czech',
                         'Marty' : 'Swiss',
                         'Bruno' : 'Italian',
                         'Socrates' : 'Greek',
                         'Marcus Aurelius' : 'Roman',
                         'Proclus' : 'Turkish',
                         'Suhrawardi' : 'Iranian',
                         'Al-Baghdadi' : 'Iraqi',
                         'Al-Ghazali' : 'Iranian',
                         'Sharpe' : 'German',
                         'Zabarella' : 'Italian',
                         'Taurellus' : 'German',
                         'Montaigne' : 'French',
                         'Ramus' : 'French',
                         'Bentham' : 'English',
                         'Hartley' : 'English',
                         'Peirce' : 'American',
                         'Whewell' : 'English',
                         'Maimon' : 'Belarusian',
                         'Godwin' : 'English',
                         'Wollstonecraft' : 'English',
                         'Sidgwick' : 'English',
                         'Husserl' : 'German',
                         'Ryle' : 'English',
                         'McTaggart' : 'English',
                         'Hartshorne' : 'American',
                         'Feyerabend' : 'Austrian',
                         'Sellars' : 'American',
                         'Tarski' : 'Polish',
                         'Findlay' : 'South_African',
                         'Cohen' : 'Canadian',
                         'Popper' : 'English',
                         'Anscombe' : 'English'
                         }

# set of cities which we should exclude
excluded_cities = {'Of', 'Most', 'University', 'Batman', 'Wayne', 'Taylor',
                   'Man', 'Plato', 'Vladimir', 'Independence', 'Richardson',
                   'Nelson', 'Shelby', 'David', 'Frederick', 'Bela', 'Roy',
                   'Columbia', 'Martin', 'Burke', 'Anderson', 'Allen',
                   'Adler', 'Arnold', 'Blackburn', 'Draper', 'Warren', 'Mobile',
                   'Abdera', 'Van'}
# set of cities that we add to the database
added_cities = {'Elea', 'Abdera', 'Miletus', 'Stagira', 'Agrigento', 'Carthage',
                'Ephesus', 'Colophon', 'Clairvaux', 'Canterbury', 'Mazitli', 'Soli', 'New York'
                }

# a cache of SEP articles
sep_cache = {}
sep_html_cache = {}

# This is the first year from which we start recording things. 
# Set this equal to the first year of the given time period. 
# The window duration indicates the number of years you want your period to show. 
first_ever_period = 1926

def main():
    global sep_cache
    citylist = load_cities_bigger_than_limit('cities.csv')
    countrylist = load_countries('countries_reduced.csv')
    phil_list_SEP = load_phil_list_SEP('philosophers.csv')
    if not ignore_pickled_data and os.path.exists('sep_cache.pickle'):
        with open('sep_cache.pickle', 'rb') as f:
            sep_cache = pickle.load(f)
            print('PICKLE_FOUND: using data from sep_cache.pickle')
    start_of_period = first_ever_period

    #window_duration = 329 # PRESOCRATIC 800 BC - 471 BC

    #window_duration = 170 # CLASSICAL 470 BC - 301 BC
    #window_duration = 600 # HELLENISTIC 300 BC - 300 AD
    #window_duration = 1199 # MEDIEVAL 301 - 1500
    #window_duration = 124 # RENAISSANCE 1501 - 1625
    #window_duration = 124 # EARLY MODERN 1 1626 - 1750
    #window_duration = 74 # EARLY MODERN 2 1751 - 1825
   
    window_duration = 20
    #1846 - 1866 
    #1866 - 1886
    #1886 - 1906
    #1906 - 1926
    #1926 - 1946
    #1946 - 1966

    increment_years = 2900 # FOR ALL TIME PERIODS IF SINGLE RUN IS DESIRED


    # loop over time periods and process data for that period, writing
    # graphs of that period
    while start_of_period <= 2022:
        end_of_period = start_of_period + window_duration
        process_period(phil_list_SEP, first_ever_period, start_of_period, end_of_period,
                       citylist, countrylist)
        start_of_period += increment_years

    with open('sep_cache.pickle', 'wb') as f: # not sure what this is or does
        pickle.dump(sep_cache, f, pickle.HIGHEST_PROTOCOL)
    print('just wrote sep_cache to pickle file sep_cache.pickle')

    # some video-making ideas, using tips from:
    # https://stackoverflow.com/questions/24961127/how-to-create-a-video-from-images-with-ffmpeg
    #print('# you can now run the following commands to make movies:')
    #print("ffmpeg -framerate 4 -pattern_type glob -i 'city_freq*_labeled.png' -c:v libx264 -pix_fmt yuv420p freq.mp4")
    #print("ffmpeg -framerate 4 -pattern_type glob -i 'phil_cities*resized.png' -c:v libx264 -pix_fmt yuv420p phil_cities.mp4")
    #print('# or you can add music with something like:')
    #print("ffmpeg -y -framerate 3 -pattern_type glob -i 'phil_cities*resized.png' -i 01-Concerto.mp3 -c:a copy -shortest -c:v libx264 -pix_fmt yuv420p phil_cities.mp4")
    #print("ffmpeg -y -framerate 3 -pattern_type glob -i 'city_freq_????.png' -i G_Dragon-Missing_you__Crayon__Heartbreaker.mp3 -c:a copy -shortest -c:v libx264 -pix_fmt yuv420p city_freq.mp4")

def process_period(phil_list_SEP, first_ever_year, start_year, end_year, citylist, countrylist):
    """Zoom in on a period from start_year to end_year and process
    philosophers born in that period."""
    print(f'==== processing period [{start_year}, {end_year}] ====')
    phil_list = trim_philosophers_in_dates(phil_list_SEP, start_year, end_year)
    # now get serious: find all Countries AND respective cities associated with all
    # philosophers 
    phil_cities = {}
    phil_countries = {}
    phil_phils = {}
    if len(phil_list) == 0:
        return
    phil_last_name_list = list(zip(*phil_list))[0]
    pcountry2phil = {}           # map country to all philosophers in that country
    for i, phil in enumerate(phil_list):
        phil_cities[phil] = find_associations(phil, citylist)
        phil_countries[phil] = find_associations(phil, countrylist)
        phil_phils[phil] = find_associations(phil, phil_last_name_list)
        # now add to the map of principal countries
        pcountry = find_principal_country(phil, countrylist)
        if pcountry in pcountry2phil:
            pcountry2phil[pcountry].append(phil)
        else:
            pcountry2phil[pcountry] = [phil]
    # now study connections between philosophers and cities,
    # countries, and principal countries
    period_index = start_year - first_ever_year
    duration = end_year - start_year
    graph_fname = f'phil_phils_pcountry_yr_since_start_{period_index:04}_plus_{duration}.dot'
    write_otherphil_graphs_with_countries(phil_list, phil_phils, pcountry2phil,
                                          graph_fname, start_year, end_year)
    print(f'# wrote graphs based on {graph_fname}')

def get_sep_article_html(url):
    if url in sep_html_cache:
        if verbose:
            print('HIT_HTML_CACHE:', url)
        return sep_html_cache[url]
    try:
        f = urllib.request.urlopen(url)
    except:
        if verbose:
            print(' ... this URL is not in SEP:', url)
        sep_html_cache[url] = None # put None in the cache
        return None
    text_bytes = f.read()
    article_text = text_bytes.decode('utf-8')
    # skip opening stuff: looks like SEP has all the good stuff before
    # the first <p>
    start_marker = '<p>'
    pos_start_marker = article_text.find(start_marker)
    article_html = article_text[pos_start_marker:] # trim off the end
    # now avoid stuff at the end, like the bibliography
    end_marker = '<div id="bibliography">'
    pos_end_marker = article_text.find(end_marker)
    article_html = article_text[:pos_end_marker] # trim off the end

    f.close()
    sep_html_cache[url] = article_html # cache the raw html
    if verbose:
        print('ADD_HTML_CACHE:', url)
    return article_html
                    
def load_cities_bigger_than_limit(fname):
    with open(fname, 'r') as f:
        lines = f.readlines()
    # lines = [c.strip() for c in citylist]
    citylist = set({})
    for line in lines[1:]:
        words = line.split(';')
        if not words[13].isdigit():
            continue
        cityname = words[2]
        if cityname in excluded_cities:
            continue
        population = int(words[13])
        if population > city_size_limit:
            # now drop those with less than the limit people
            if verbose:
                print('ACCEPTING_CITY:', cityname, population)
            citylist.add(cityname)
    # add the special old cities
    citylist = citylist.union(added_cities)
    return list(citylist)

#load country demonyms from countries.csv
def load_countries(fname):
    with open (fname, 'r') as f:
        lines = f.readlines()
    countrylist = set()
    for line in lines[1:]:
        if line[0] == '#':
            continue            # skip comments
        words = line.split(',')
        country = words[1].strip('\n').strip()
        if '*' in words[1] or  " " in words[1] or words[1].isspace():
            if verbose:
                print(f'SKIPPING: <{country}>')
            continue
        else:
            if verbose:
                print(f'ADDING: <{country}>')
            countrylist.add(country)
    return list(countrylist)

def find_associations(name_pair, citylist):
    """Scan the SEP entry for that philosopher's name, and sees if the
    words in the entry match any cities or countries or philosophers.
    For now the variable is called "citylist" but we should change it
    to be a list of possible associates."""
    (last_name, hyphen_name) = name_pair # unpack the info we had about this philosopher
    if verbose:
        print('FIND_ASSOC_GEOGRAPHY', last_name, "...", end="")
    assoc_cities = []
    # load the text for that philosopher
    if last_name in sep_cache: # try to use cache
        if verbose:
            print('...sep_cache HIT...', end="")
        sep_entry = sep_cache[last_name]
    else:
        sep_entry = get_sep_article_text(last_name)
        # add it to our cache so that we don't have to re-fetch it
        sep_cache[last_name] = sep_entry
    # now go through each word in the article, and look for city names
    for word in sep_entry.split():
        word_stripped = word.strip(""":;/=+.,()[]"'<>!?-_""")
        if word[0].islower():
            continue
        if word_stripped in citylist:
            assoc_cities.append(word_stripped)
    if verbose:
        print(' ...', set(assoc_cities))
    return assoc_cities

def phil_name2url(phil_name):
    url = 'https://plato.stanford.edu/entries/' + phil_name.lower() + '/'
    return url


def get_sep_article_text(phil_name):
    url = phil_name2url(phil_name)
    sep_html = get_sep_article_html(url)
    if not sep_html:           # try nicknames if necessary
        if phil_name in AKA_dict:
            # trick to turn single entries into a list
            aka_list = (AKA_dict[phil_name] if type(AKA_dict[phil_name]) == type([])
                        else [AKA_dict[phil_name]])
            for othername in aka_list:
                print('TRY_OTHERNAME:', othername)
                url = phil_name2url(othername)
                sep_html = get_sep_article_html(url)
                if sep_html:
                    break
    if not sep_html:
        return ''
    # first remove all HTML using beautiful soup
    soup = BeautifulSoup(sep_html, "html.parser")
    sep_entry = soup.get_text()
    if verbose:
        print('...FOUND!')
    return sep_entry

def load_philosophers(fname):
    """Loads a list of philosophers from a file, returning
    a list of triples (last_name, hyphen_name, birthyear)."""
    with open(fname, 'r') as f:
        lines = f.readlines()
    phil_list = []
    for line in lines[1:]:
        if line[0] == '#':
            continue
        words = line.split(',')
        words = [w.strip() for w in words]
        name = words[0]
        birthdate_str = words[1]
        # we might have '?' in the birth year meaning it is uncertain,
        # so we strip that here.  IMPROVEME: at some point this
        # uncertainty should be used in some useful way in the code
        birthdate_str = birthdate_str.strip('?')
        if verbose:
            print('birthdate_str:', birthdate_str, '   name:', words[0])
        if "BC" in birthdate_str: # if it's BC make it negative
            birthyear = -int(birthdate_str.split()[0])
        else:
            birthyear = int(birthdate_str)
        name = words[0]
        if "of" in name:
            name = name.strip('of')
        elif "The" in name:
            name = name.strip('The')
        #print(name)
        if " " in name: # separates first and last name for url insertion
            lastname = name.split(' ')[-1] #outputs last name
            hyphen_name = name.replace(' ', '-') #puts hyphens between names - some SEP entries do this
        else:
            lastname = name
            hyphen_name = name
        # if they are in the excluded list, don't continue
        if lastname in excluded_philosophers:
            continue
        # if we have reached this point, append the pair to the list
        birthyear = words[1]
        phil_list.append((lastname, hyphen_name, birthyear))
    return phil_list


def trim_philosophers_in_dates(phil_list_SEP, start_year, end_year):
    """Loads a list of philosophers from a file (triplets with lastname,
    hyphenname, birthyear), and return only those that were born in the
    given date range."""
    phil_list = []
    for lastname, hyphen_name, birthyear_str in phil_list_SEP:
        # we might have '?' in the birth year meaning it is uncertain,
        # so we strip that here.  IMPROVEME: at some point this
        # uncertainty should be used in some useful way in the code
        birthyear_str = birthyear_str.strip('?')
        if verbose:
            print('birthyear_str:', birthyear_str, '   name:', words[0])
        if "BC" in birthyear_str: # if it's BC make it negative
            birthyear = -int(birthyear_str.split()[0])
        else:
            birthyear = int(birthyear_str)
        # now only append them if they are in the range
        if birthyear >= start_year and birthyear <= end_year:
            phil_list.append((lastname, hyphen_name)) # skip the date
    return phil_list

def trim_phil_list_for_SEP(phil_list_big):
    """Take a list of philosophers and trim it to just those with SEP
    entries.  This will cache articles as it goes along, which can be
    useful to other parts of a program.
    """
    trimmed_list = []
    for phil in phil_list_big:
        article_html = get_sep_article_html(phil_name2url(phil[0]))
        if not article_html or len(article_html) == 0:
            print(phil, '... SKIP (no SEP entry)')
            continue            # no article; skip it!
        if '<title>Not Yet Available</title>' in article_html:
            print(phil, '... SKIP (SEP entry says Not Yet Available)')
            continue
        print(phil, '... ACCEPT (yes SEP entry)')
        trimmed_list.append(phil)
    if not ignore_pickled_data:
        with open('phil_list_SEP.pickle', 'wb') as f:
            pickle.dump(trimmed_list, f, pickle.HIGHEST_PROTOCOL)
    return trimmed_list


def find_frequency_of_cities(phil_list, citylist, phil2cities):
    """Take a list of cities, and a dictionary that maps philosophers to
    associated cities, and makes a frequency mapping of how many times
    the cities occur."""
    city_freq = {}
    for phil in phil_list:
        for city in phil2cities[phil]:
            if city in city_freq:
                city_freq[city] += 1
            else:
                city_freq[city] = 1
    return city_freq

def write_graphs(phil_list, citylist, phil2cities, phil_countries, fname, start_year, end_year):
    """Prepares a graphviz file that would map philosophers to their
    cities (or countries or other philosophers)."""
    graph_strings = ''
    # make graphs of all the (cities, or countries, or other
    # philosophers associated with the philosophers in phil_list
    for phil in phil_list:
        phil_lastname = phil[0]
        # for this philosopher look at the (cities, or countries, or
        # other philosophers that match this one)
        for city in set(phil2cities[phil]):
            # note that the match needs to be on the last name because
            # that's how the list of associated philosophers is stored
            if phil_lastname != city:
                graph_strings += f'    "{phil_lastname}" -> "{city}";\n'
    if len(graph_strings) == 0:
        print(f'NO connections for period starting {start_year:04d}')
        return
    start_graphviz_file(fname, start_year)
    with open(fname, 'a') as f:
        f.write(graph_strings)
    end_graphviz_file(fname)
    # print(f'Wrote the file {fname}; now running these commands to make graphical files:')
    file_endings = ('png', 'pdf')
    # use a trick from
    # https://stackoverflow.com/questions/14784405/how-to-set-the-output-size-in-graphviz-for-the-dot-format
    # to output our pictures to have a regular size
    for filetype in file_endings:
        cmd = f'dot -T{filetype} -Gsize=9,15\! -Gdpi=100 -O {fname}'
        os.system(cmd)
    # make the PNG file have a uniform size with some ImageMagick
    # cleverness
    cmd = f'convert {fname}.png -gravity center -background white -extent 900x1500 {fname}_resized_notext.png'
    # print(cmd)
    os.system(cmd)
    # write a label into the PNG file
    yr_str = (str(start_year)+'CE') if start_year >= 0 else (str(-start_year)+'BCE')
    cmd = f"convert {fname}_resized_notext.png -gravity SouthWest -pointsize 20 -annotate +10+10 'period_{yr_str}to{end_year}' {fname}_resized.png"
    # print(cmd)
    os.system(cmd)
    print(f'/bin/rm -f {fname}.png {fname}_resized_notext.png')
    os.system(f'/bin/rm -f {fname}.png {fname}_resized_notext.png')

def write_otherphil_graphs_with_countries(phil_list, otherphil_dict, pcountry2phil,
                                          fname, start_year, end_year):
    """Prepares a graphviz file that would map philosophers to other
    philosophers, with subgraph clusters based on countries."""
    if len(phil_list) == 0:
        return
    # start by iterating over countries
    graph_str = ''
    graph_str_not_both = ''
    country_list = pcountry2phil.keys()
    countries_analytical = [country for country in country_list
                            if country in ['British', 'Irish', 'American', 'Australian', 'New_Zelander',
                                           'Canadian']]
    countries_continental = [country for country in country_list if country not in countries_analytical]
    graph_str += '    subgraph cluster_Analytical {\n    rank="min";\n'
    for pcountry in countries_analytical:
        s, not_both_same_country = make_country_graph_str(pcountry, pcountry2phil, otherphil_dict)
        graph_str += s
        graph_str_not_both += not_both_same_country
    graph_str += 'label="Analytical";    }'
    graph_str += '    subgraph cluster_Continental {\n    rank="max";\n'
    for pcountry in countries_continental:
        s, not_both_same_country = make_country_graph_str(pcountry, pcountry2phil, otherphil_dict)
        graph_str += s
        graph_str_not_both += not_both_same_country
    graph_str += 'label="Continental";    }'
    start_graphviz_file(fname, start_year)
    with open(fname, 'a') as f:
        f.write(graph_str)
        f.write(graph_str_not_both)
    end_graphviz_file(fname)
    # file_endings = ('pdf', 'png')
    # file_endings = ('pdf', 'svg', 'png')
    file_endings = ('png',)
    # use a trick from
    # https://stackoverflow.com/questions/14784405/how-to-set-the-output-size-in-graphviz-for-the-dot-format
    # to output our pictures to have a regular size
    for filetype in file_endings:
        cmd = f'dot -T{filetype} -Gsize=9,15\! -Gdpi=100 -O {fname}'
        os.system(cmd)
    # make the PNG file have a uniform size with some ImageMagick
    # cleverness
    cmd = f'convert {fname}.png -gravity center -background white -extent 900x1500 {fname}_resized_notext.png'
    # print(cmd)
    os.system(cmd)
    # write a label into the PNG file
    start_yr_str = (str(start_year)+'CE') if start_year >= 0 else (str(-start_year)+'BCE')
    end_yr_str = (str(end_year)+'CE') if end_year >= 0 else (str(-end_year)+'BCE')
    cmd = f"convert {fname}_resized_notext.png -gravity SouthWest -pointsize 20 -annotate +10+10 'period_{start_yr_str} to {end_yr_str}' {fname}_resized.png"
    # print(cmd)
    os.system(cmd)
    os.system(f'/bin/rm -f {fname}.png {fname}_resized_notext.png')

def make_country_graph_str(pcountry, pcountry2phil, otherphil_dict):
    """Make the subgraph cluster for a single country."""
    not_both_this_pcountry = ''
    s = f'    subgraph cluster_{pcountry}' +  '{\n'
    s += '    rank="same";\n'
    s += '    node [style=filled,color='
    s += get_fill_colormap(pcountry)
    s += """];
        style=filled;
        color=""" + get_shade_colormap(pcountry) + """;
"""
    for phil in pcountry2phil[pcountry]:
        # first make a node for this philosopher
        s += f'        "{phil[0]}";\n'
        # then make all the edges to their associated philosophers
        for associated_phil in set(otherphil_dict[phil]):
            if associated_phil == phil[0]:
                continue        # don't refer to yourself
            lastnames = list(zip(*(pcountry2phil[pcountry])))[0]
            if associated_phil in lastnames:
                s += f'        "{phil[0]}" -> "{associated_phil}";\n'
            else:
                not_both_this_pcountry += f'        "{phil[0]}" -> "{associated_phil}";\n'
    s += f'        label="{pcountry}";\n'
    s += "    }\n";
    return s, not_both_this_pcountry

def start_graphviz_file(fname, start_year):
    with open(fname, 'w') as f: # zero it out
        f.write("""digraph philosophers_to_cities {
    rankdir="LR";
""")
        f.write(f'    // start_year: {start_year}\n')

def end_graphviz_file(fname):
    with open(fname, 'a') as f:
        f.write('}\n')

def plot_city_freq(city_freq, period_index, start_year):
    if len(city_freq) == 0:     # don't make graph if nothing's here
        return
    plt.clf()
    plt.bar(range(len(city_freq)), list(city_freq.values()), align='center')
    plt.xticks(range(len(city_freq)), list(city_freq.keys()), rotation=45)
    plt.title('Frequency of Cities in Association with Philosophers')
    plt.xlabel('Cities', labelpad = 10) 
    plt.ylabel('Frequency')
    fname = f'city_freq_{period_index:04}.png'
    plt.savefig(fname)
    yr_str = (str(start_year)+'CE') if start_year >= 0 else (str(-start_year)+'BCE')
    cmd = f"convert {fname} -gravity SouthWest -pointsize 10 -annotate +10+10 'period_starting_{yr_str}' {fname[:-4]}_labeled.png"
    os.system(cmd)


def make_principal_country_map(phil_list, countrylist):
    """Takes a list of philosophers and makes a map from each philosopher
    to their most likely country of origin (using the rather
    simplistic heuristics in find_principal_country().
    """
    # currently unused; why???
    pcountry_map = {}
    for phil in phil_list:
        principal_country = find_principal_country(phil, countrylist)
        if verbose:
            print('principal_country:', phil, principal_country)
        pcountry_map[phil] = principal_country
    return pcountry_map


def find_principal_country(phil_name, country_list):
    """Find the country that seems most likely to be the home country of a
    philosopher.  We start from the SEP entry and try some simple
    heuristics, like "is a British" or "is a French".  If that does
    not work we look for most frequent mention of a nationality, and
    use first occurrence of a nationality word in the sep_entry as a
    tiebreaker.
    """
    # first see if there's a hard-coded entry -- this is for people
    # like Hegel for whom automatic parsing does not work, since Greek
    # appears more often than German in his article
    if phil_name[0] in hardcoded_country_map:
        return hardcoded_country_map[phil_name[0]]
    occurrence_map = {} # save how many times each country occurs in the text
    for country in country_list:
        sep_entry = get_sep_article_text(phil_name[0])
        # first attempt: things like "is a British" or "was a ..."
        if phil_is_country_explicit(phil_name, country, sep_entry):
            if verbose:
                print(f'find_principal_country_EXPLICIT: {phil_name} <{country}>')
            return country
        # second attempt: look for how many occurrences of that
        # country appear in this text
        occurrence_map[country] = sep_entry.count(country)
    # at this point, if we have not returned, we have a dictionary
    # mapping countries to how many times they occur
    max_country = max(occurrence_map, key=occurrence_map.get)
    if verbose:
        print(f'find_principal_country_majority: {phil_name} ***{max_country}*** ', end="")
        for k in occurrence_map:
            if occurrence_map[k] > 0:
                print((k, occurrence_map[k]), ' ', end="")
        print('')
    if occurrence_map[max_country] == 0:
        print(f'find_principal_country_majority: {phil_name} ***{max_country}*** ', end="")
        for k in occurrence_map:
            if occurrence_map[k] > 0:
                print((k, occurrence_map[k]), ' ', end="")
        print('')
        return 'NoCountry'
    else:
        return max_country


def phil_is_country_explicit(phil_name, country, sep_entry):
    """See if this philosopher is in that country explicitly, i.e. there
    is a phrase like 'was a British' or 'was a Greek' or 'was an
    arabic' ...  This is the strongest heuristic of nationality.
    Return True if so, and False otherwise.
    """
    countrynamelist = [country]
    if country in AKA_countries:
        countrynamelist += AKA_countries[country]
    # print(f'ENTER_explicit({phil_name} -- countrynamelist:', countrynamelist)
    for country_to_try in countrynamelist:
        for belonging_str in ('is a', 'was a', 'is an', 'was an'):
            search_str = belonging_str + ' ' + country_to_try
            pos = sep_entry.find(search_str)
            if pos != -1:
                if verbose:
                    print(search_str, pos)
                    print(f'EXPLICIT_{country_to_try}:', phil_name, country_to_try, pos)
                return True
    return False

def get_fill_colormap(country):
    return 'lightblue'

def get_shade_colormap(country):
    return 'lightgrey'

def load_phil_list_SEP(fname):
    """Load those philosophers that have SEP entries.  We do this by
    loading the full file and removing those who don't have SEP entries.."""
    pickle_fname = 'phil_list_SEP.pickle'
    if not ignore_pickled_data and os.path.exists(pickle_fname):
        # restore from a pickled file, if we have it
        print(f'PICKLE_FOUND: using data from file {pickle_fname}')
        with open(pickle_fname, 'rb') as f:
            phil_list_SEP = pickle.load(f)
            return phil_list_SEP
    # if it's not in cache we load it from file, and then trim it
    # based on being in the SEP
    phil_list_big = load_philosophers('philosophers.csv')
    # phil_list_big = phil_list_big[:100]
    print(f'loaded {len(phil_list_big)} records from philosophers.csv')
    if len(phil_list_big) == 0:
        print(f'empty list; not processing {start_year}..{end_year}')
        return
    phil_list_SEP = trim_phil_list_for_SEP(phil_list_big) # trim it to just those with SEP entries
    print(f'trimmed to just those in SEP: {len(phil_list_SEP)} records')
    if len(phil_list_SEP) == 0:
        print(f'empty SEP-available list; not processing {start_year}..{end_year}')
        return
    # now that we've read it all in, let's save the people we picked
    # to a file
    if not ignore_pickled_data:
        with open('phil_list.pickle', 'wb') as f:
            pickle.dump(phil_list_SEP, f, pickle.HIGHEST_PROTOCOL)
    return phil_list_SEP


if __name__ == '__main__':
    main()

# sep_project

<h1><b>Citation Networks of the Stanford Encyclopedia of Philosophy</b></h1>

This program creates various visual citation networks for multiple parameters within the Stanford Encyclopedia of Philosophy. The program takes Stanford Encyclopedia of Philosophy philosopher entries and identifies citations of other philosophers and geographic locations and creates plots that show corresponding clusters. Lastly, it identifies which philosophers are analytic or continental. The objective of the program and plots is to demonstrate the correspondence between philosophers based on ideas and locations in a basic and visual manner.  

<h2><b>Installation</b></h2>

Clone this repository:
```bash
git clone https://codeberg.org/chromedome/sep_project
```
<h2><b>Usage</b></h2>

Please note that you must specify the time period for which you want to produce a graph in the program. The increments I used are included in the code as comments. 

Once you are in the cloned directory, you can run the program in your command line using: 
```bash
python3 main.py
```
or 
```bash
./main.py 
```
Running the program will produce a .dot and a .png file. The .png file is the graph. You can convert the .dot file into a 
.pdf version of the graph using:
```bash
dot -Tpdf -O <filename.dot>
```
Currently, the program only outputs philosopher-to-philosopher citation visualization. However, it does have the capacity to produce philosopher-to-city and philosopher-to-country citation visualizations as well. The functions that execute the production of these graphs have been commented out, but can be uncommented should you want the aforementioned graphs. 

<h2><b>Materials</b></h2>

<a href='https://plato.stanford.edu/index.html'>The Stanford Encyclopedia of Philosophy</a> is free and easily accesible through a browser. Looking through some of the philosopher entries available can inform on some of the connections and clusters in the visualizations. Additionally, if you are interested in how the plots were made, visit this <a href='https://www.graphviz.org/pdf/dotguide.pdf'>graphiz <i> dot</i> tutorial</a> and reference the code. It might also be helpful to view the .dot file produced from running the program. 

<h2><b>Contributions</b></h2>

There are multitudes of possible extensions to this program and the analysis it undertakes. One that might be of particular interest would be an encompassing meta-analysis using other online philosophical encyclopedias (of which there are many) to compare with what has been produced here. With regards to this porgram, the code uses rather simplistic and heuristic methods which could certainly be improved with more adavanced techniques. Please feel free to send a pull request and/or copy the program to make changes and improvements. 